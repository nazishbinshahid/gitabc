//
//  LocationManager.swift
//  Socio
//
//  Created by soc-macmini-55 on 07/07/17.
//  Copyright © 2017 Click-Labs. All rights reserved.
//
// By NAZISH
//ABC()
import UIKit
import Foundation
import CoreLocation

class LocationManager: NSObject {
  
  var locationManager = CLLocationManager()
  fileprivate static let  locationInstance = LocationManager()
  class func  shareInstance() -> LocationManager {
    return locationInstance
  }
  
  func configureLocationServices() {
    self.locationManager.delegate = self
    let authorizationStatus = CLLocationManager.authorizationStatus()
    switch authorizationStatus {
    case .notDetermined:
      // Request authorization.
      self.locationManager.requestWhenInUseAuthorization()
      break
    
     default:
      break
    }
  }
  
 func startLocationTracker() {
    self.locationManager.distanceFilter = 50
    self.locationManager.startUpdatingLocation()
 
 }
  
  func stopLocationTracker() {
       self.locationManager.stopUpdatingLocation()
     }
  
  func trackChandigarh() {
     print("location")
    var a = 10
    print(a)
    var b = 5
    print(b)
    var c = 5
    print(c)
    var d = 5
    print(d)
  }

}

extension LocationManager : CLLocationManagerDelegate {
  func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
    
    if let lastLocation = locations.last {
      print(lastLocation)
      print(lastLocation.timestamp)
      //  BackgroundTask.shareInstanceFunction().beginBackgroundTask("\(lastLocation.timestamp)")
      let locationDict = ["latitude" : lastLocation.coordinate.latitude, "longitude" : lastLocation.coordinate.longitude, "timeStamp" : lastLocation.timestamp] as [String : Any]
      UserDefaults.standard.set(locationDict, forKey: "Location")
      
    }
  }
  
  func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
    print(error)
  }
  
  func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
    print(status)
    switch status {
    case .authorizedAlways, .authorizedWhenInUse:
      self.startLocationTracker()
      break
    case .denied, .restricted:
      self.stopLocationTracker()
      
      break
    default:
      break
    }
  }
  
}
